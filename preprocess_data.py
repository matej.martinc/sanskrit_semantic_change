import pandas as pd
import os
import argparse


def extract_lemmatized_sentences_from_tsv(file, remove_corrupted=True):
    """Extracts the lemmatized sentences from the original files."""
    data = pd.read_csv(file)
    text = ''
    prev_id = 0
    for index, row in data.iterrows():
        #if str(row['word']) == '</s>' or str(row['word']) == '/':
        if pd.isna(row['WordID']):
            continue
        try:
            id = int(row['WordID'])
            if id < prev_id or str(row['word']) == '/':
                if not text.strip().endswith("/") and len(text.strip()) > 0:
                    text += "/\n"
                elif text.strip().endswith("/") and len(text.strip()) > 0:
                    text = text.strip()
                    text += "\n"
            if str(row['PoS']) == 'punct':
                continue
            elif pd.isna(row['lemma']):
                continue
            elif str(row['lemma']) == "CORRUPTED":
                if remove_corrupted:
                    continue
                else:
                    text += "***UNK***"
                    text += " "
            else:
                if str(row['lemma']) == '//':
                    text += '/'
                else:
                    text += str(row['lemma'])
                text += " "
            prev_id = id
        except: continue
    return text


def transform_from_csv_to_txt(csv_path, chunk):
    files = os.listdir(csv_path)
    data = []
    for f in files:
        path = os.path.join(csv_path, f)
        t = extract_lemmatized_sentences_from_tsv(path)
        t = t.split('\n')
        for sent in t:
            if len(sent) > 0:
                data.append((chunk, f, sent))

    output_df = pd.DataFrame(data, columns=['period_name', 'source', 'text'])
    return output_df




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_path",
                        default='data/CorporaJuly2023',
                        type=str,
                        help="Path to the entire Sanskrit corpus containing Buddhist and NonBuddhist texts")
    parser.add_argument("--output_path",
                        default='data/sanskrit_buddhist_nonbuddhist_corpus.tsv',
                        type=str,
                        help="Path to the entire Sanskrit corpus containing Buddhist and NonBuddhist texts")

    args = parser.parse_args()
    df1 = transform_from_csv_to_txt(os.path.join(args.data_path, "BuddhCorpusJuly2023"), 'buddhist')
    df2 = transform_from_csv_to_txt(os.path.join(args.data_path, "RefCorpusJuly2023/MangalamRefCorpus"), 'nonbuddhist')
    df3 = transform_from_csv_to_txt(os.path.join(args.data_path, "RefCorpusJuly2023/RefCorpusFromConlluFiles"), 'nonbuddhist')
    df_data = pd.concat([df1, df2, df3])
    df_data.to_csv(args.output_path, sep='\t', encoding='utf8', index=False)

