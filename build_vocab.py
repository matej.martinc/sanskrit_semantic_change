from collections import defaultdict
import pandas as pd
import os
import dill
import argparse
from transformers import AutoTokenizer

w_tokenizer = AutoTokenizer.from_pretrained("Matej/bert-small-buddhist-nonbuddhist-sanskrit", use_fast=True)
w_tokenizer.pad_token = '[PAD]'
w_tokenizer.unk_token = '[UNK]'
w_tokenizer.cls_token = '[CLS]'
w_tokenizer.sep_token = '[SEP]'
w_tokenizer.mask_token = '[MASK]'


def get_stopwords():
    df = pd.read_csv('data/SktStopwordsJuly2023.csv', encoding='utf8', sep=',')
    sw = df['Var1'].tolist()
    sw = [w.strip() for w in sw]
    return set(sw)


class Vocab():
    def __init__(self, min_freq):
        self.docs = defaultdict(list)
        self.lemmatized_docs = defaultdict(list)
        self.chunks = []
        self.meta = defaultdict(list)
        self.min_freq = min_freq

    def add(self, doc, lemmatized_doc, chunk, source):
        if chunk not in self.chunks:
            self.chunks.append(chunk)
        self.docs[chunk].append(doc)
        self.lemmatized_docs[chunk].append(lemmatized_doc)
        self.meta[chunk].append(source)

    def make_vocab(self, output_dir):
        print('making_vocab')
        all_freqs = []
        freqs = defaultdict(int)
        sw = get_stopwords()
        for chunk in self.chunks:
            print("chunk: ", chunk)
            chunk_freqs = defaultdict(int)
            count_words = 0
            for doc in self.lemmatized_docs[chunk]:
                for sent in doc.split(" <eos> "):
                    for word in sent.split():
                        is_digit = word.isdigit()
                        if not is_digit:
                            if len(word) > 2 and word.lower() not in sw:
                                chunk_freqs[word] += 1
                                freqs[word] += 1
                                count_words += 1
            all_freqs.append((chunk_freqs, count_words))
        print('All vocab size: ', len(freqs))

        filtered_freqs = []
        for word, freq in freqs.items():
            allow = True
            diff = []
            for chunk_freq, _ in all_freqs:
                diff.append(chunk_freq[word])
                if chunk_freq[word] < self.min_freq :
                    allow = False
                    break
            if allow:
                filtered_freqs.append((word, freq))

        print('Len filtered freq: ', len(filtered_freqs))
        self.freqs = []
        freqs = sorted(filtered_freqs, key=lambda x: x[1], reverse=True)
        with open(os.path.join(output_dir, 'vocab_list_of_words.csv'), 'w', encoding='utf8') as f:
            f.write('word,mean\n')
            for w, freq in freqs:
                w = w_tokenizer.tokenize(w)
                w = "".join(w).replace('##', '')
                f.write(w + ',' + str(freq) + '\n')
                self.freqs.append((w, freq))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_path",
                        default='data/sanskrit_buddhist_nonbuddhist_corpus.tsv',
                        type=str,
                        help="Path to the tsv file containing the data")
    parser.add_argument("--output_dir",
                        default='vocab',
                        type=str,
                        help="Path to the folder that will contain generated output vocab and frequency files")
    parser.add_argument("--min_freq",
                        default=10,
                        type=int,
                        help="Minimum frequency of the word in a specific chunk to be included in the vocabulary")

    args = parser.parse_args()
    df_data = pd.read_csv(args.data_path, sep='\t', encoding='utf8')
    df_data = df_data.groupby(['period_name', 'source'])['text'].apply(' <eos> '.join).reset_index()
    df_data = df_data.sample(frac=1, random_state=123)
    print("Corpus shape: ", df_data.shape)

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    vocab = Vocab(args.min_freq)
    all_data = []
    all_sents = []
    all_sources = []
    source_counts = defaultdict(int)
    for idx, row in df_data.iterrows():
        # chunk = str(row['year'])
        chunk = str(row['period_name'])
        meta = [row['source']]
        source_counts[chunk] += 1
        text = row['text']
        lemmatized_text = row['text']
        sents = text.split(' <eos> ')
        lemmatized_sents = lemmatized_text.split(' <eos> ')
        for sent, lemmatized_sent in zip(sents, lemmatized_sents):
            if len(sent.split()) < 128 and len(sent.split()) > 2:
                vocab.add(sent, lemmatized_sent, chunk, meta)
            all_sents.append(sent)
        all_sources.append(chunk)
    print('Sources in vocab: ', list(set(all_sources)))

    vocab.make_vocab(args.output_dir)
    with open(os.path.join(args.output_dir, 'vocab.pickle'), 'wb') as handle:
        dill.dump(vocab, handle)

    print('Done building vocab.')





